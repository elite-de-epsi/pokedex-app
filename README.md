# Project Pekodex Python (PPP)

**<u>Bienvenue sur notre Projet de Pekodex de Pekomens !</u>**

Tout d'abord, une brève présentation du projet s'impose :

    Dans le cadre de la troisième année de bachelor à l'EPSI nous devions réaliser un **pékodex** basé sur une **API** à notre disposition en **Python**.

    En effet, Python peut être utilisé pour divers projets, grâce aux nombreuses fonctionnalités qu'il offre par défaut et avec des bibliothèques standards couvrant presque toutes les tâches de programmation. On peut effectuer des **calculs scientifiques**, traiter des images ou développer des interfaces ou des protocoles de systèmes d’exploitation. 

    Python est disponible pour presque tous les systèmes d'exploitation, y compris les systèmes d'exploitation basés sur UNIX, Windows, macOS, iOS et Android. Les entreprises du monde entier utilisent Python pour l’**intelligence artificielle** et **l’apprentissage automatique**, le **développement de sites web**, l’**informatique scientifique et numérique, les jeux** et bien d’autres utilisations. Python est suffisamment fonctionnel pour s’assembler avec du code **écrit dans d’autres langages de programmation**. Il est donc possible d'intégrer un projet Python dans des frameworks de différents langages, et inversement. On utilise notamment des langages comme le HTML ou le CSS directement dans le framework Django qu'on a utilisé et qu'on présentera ultérieurement.

## Présentation du projet :

    Dans notre cas, nous avons utilisé Python pour développer une application web. Pour rappel, une API a été mise à notre disposition. Cette application devait lister 151 pekomens, ainsi que leurs détails avec leur nom, une photo et d’autres informations supplémentaires. 

    L’objectif  est de pouvoir faire **naviguer notre utilisateur** à travers notre application afin qu’il puisse **sélectionner un pekomen de son choix** en ayant à sa disposition **ses différentes caractéristiques** dans le but qu'il puisse le prendre pour **former son équipe** par la suite. L’utilisateur va donc pouvoir créer sa propre team, ékip formés de tous les pekomens avec les caractéristiques qu'il aura choisi. 

    Comme nous l'avons évoqué, notre outil dispose d’une fonctionnalité permettant d’ajouter 6 pekomens dans son ékip. Mais quel pokémon choisir il y en a trop. Eh oui ! C’est pour cela que notre application possède aussi une fonctione de recherche ce qui va te permettre de faire le tri et chercher plus vite le pekomen qu’il te faut pour ta team. 

## Installation du programme :

    Pour installer le projet, il faut dans un premier temps installer Python. Ce langage possède de nombreuses versions. Nous avons pris la version 3.10. De plus, ce langage permet d’être utilisé sur de nombreux environnements qui soient seulement adapté à Python comme Pycharm,InteliJ ou Visual Studio Code par exemple.

    Ensuite, nous avons créé un git pour ce projet. On peut récupérer ce code en faisant un clone du git et le récupérer sur un nouveau projet.  Le clone est directement possible par différents IDE mais vous pouvez vous référez à ce tuto : [git clone | Atlassian Git Tutorial](https://www.atlassian.com/fr/git/tutorials/setting-up-a-repository/git-clone). Enfin vous devez installer Django via la commande :

```
pip install Django
```

    Quand toutes les installations sont faites, et que le projet est bien configuré. Vous pouvez ensuite éxécuter la commande suivante : 

```python
python manage.py runserver
```

    Cela va permettre de lancer votre application, vous avez ensuite à cliquer sur le lien avec une url que vous propose votre IDE qui sera celle-ci :

```
http://127.0.0.1:8000/
```

## Choix technique :

    Pour réaliser ce projet il nous fallait un affichage capable de répondre à notre besoin. Récupérer les informations sur l’API, lister les pokémons, voir leurs détails… Plusieurs outils répondent à ce besoin comme **Tkinter**, **Pyramid**, **TurboGears, Django ...** 

    Cependant, comme la plupart des personnes du groupes sont à l'aise avec la pragrammation web que ce soit en front ou en back nous avons choisi **Django**. En effet, nous avons hésiter avec Tkinter mais il s'agissait d'un vote de groupe. Tkinter permet seulement de faire des application qui ne sont pas accessible sur le web et c'était un de nos critères. Une personne du groupe est très à l'aise avec le front dans les application web d'où notre choix. 

    Django est un framework récent écrit en Python et distribué gratuitement. De plus, il est plébiscité par énormément de leaders du Web et du Cloud comme le Guardian, le New York Times, Instagram et Pinterest pour des raisons quasi évidentes de performance et de confort. Sa communauté de développeurs est très active. Parmi les atouts majeurs de ce framework on compte les éléments suivants :

- Les mises à jour fréquentes

- Son architecture MVC

- Les vues génériques proposées par défaut

**Amusez-vous bien et créer la meilleure team pour battre vos amis !**