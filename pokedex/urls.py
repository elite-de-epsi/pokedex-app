from django.urls import path

from . import views

urlpatterns = [
    path('', views.pekomens, name='pekomens'),
    path('ekips/', views.ekips, name='ekips'),
    path('ekips/set_selected_ekip/<int:ekip_id>', views.set_selected_ekip, name='set_selected_ekip'),
    path('ekip/<int:team_id>/', views.ekip, name='ekip'),
    path('ekip/create/', views.ekip_create, name='ekip_create'),
    path('ekip/create/add_pokemon/<int:pokemon_id>', views.add_pokemon, name='add_pokemon'),
    path('ekip/create/del_pokemon/<int:pokemon_id>', views.del_pokemon, name='del_pokemon'),
    path('ekip/create/save_ekip', views.save_ekip, name='save_ekip'),
    path('pekomens/', views.pekomens, name='pekomens'),
    path('pekomen/<int:pokemon_id>/', views.pekomen, name='pekomen_detail'),
    path('pekomens/search/', views.search, name='search')
]
