from django.db import models
from django.forms import forms


class Pokemon(models.Model):
    name = models.CharField(max_length=50)
    image = models.CharField(max_length=2000)
    objectId = models.CharField(max_length=50)

    def __init__(self, name, image, objectId):
        self.name = name
        self.image = image
        self.objectId = objectId


class Pokemon_detail(models.Model):
    objectId = models.IntegerField()
    name = models.CharField(max_length=50)
    abilities = []
    types = []
    stats = []
    sprites = models.CharField(max_length=50)
    weight = models.CharField(max_length=50)
    height = models.CharField(max_length=50)
    base_experience = models.CharField(max_length=50)

    def __init__(self, objectId, name, abilities, types, stats, sprites, weight,
                 height, base_experience):
        self.objectId = id
        self.name = name
        self.abilities = abilities
        self.types = types
        self.stats = stats
        self.weight = weight
        self.height = height
        self.base_experience = base_experience
        self.sprites = sprites

class Pokemon_detail_stat(models.Model):
    name = models.CharField(max_length=200)
    value = models.CharField(max_length=50)

    def __init__(self, name, value):
        self.name = name
        self.value = value

class Ekip(models.Model):

    name = models.CharField(max_length=50)
    objectId = models.CharField(max_length=50)
    pokemons = []

    def __init__(self, objectId, name, pokemons):
        self.objectId = objectId
        self.name = name
        self.pokemons = pokemons


class NameTeamForm(forms.Form):
    name = forms.Field(label='Nom de ekip')


class SearchForm(forms.Form):
    search = forms.Field(label='Rechercher un pokemon')
