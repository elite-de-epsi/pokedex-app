import requests
from django.http import HttpResponse
from django.shortcuts import render, redirect

from pokedex.models import Pokemon, Ekip, NameTeamForm, SearchForm, Pokemon_detail, Pokemon_detail_stat

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


all_pokemons = []
searched_pokemons = []
create_selected_pokemons = []
ekip_list = []
ekip_id_seq = 1
selected_ekip = None


def ekips(request):
    global ekip_list
    global selected_ekip
    if selected_ekip is None and len(ekip_list) > 0:
        selected_ekip = ekip_list[0]

    return render(request, 'ekips.html', {
        'ekip_list': ekip_list,
        'selected_ekip': selected_ekip
    })


def ekip(request, team_id):
    return HttpResponse("You're looking at team %s." % team_id)


def ekip_create(request):
    global create_selected_pokemons
    global all_pokemons

    if len(all_pokemons) <= 0:
        response_list = requests.get('https://pokeapi.co/api/v2/pokemon/?limit=151')
        pokemon_list = response_list.json()
        for global_pokemon in pokemon_list["results"]:
            response_details = requests.get(global_pokemon["url"])
            pokemon_details = response_details.json()
            all_pokemons.append(
                Pokemon(
                    global_pokemon["name"],
                    pokemon_details["sprites"]["other"]["home"]["front_default"],
                    pokemon_details["id"]
                )
            )

    selected_pokemons_object = []
    for select_id in create_selected_pokemons:
        for poke in all_pokemons:
            if poke.objectId == select_id:
                selected_pokemons_object.append(poke)

    return render(request, 'ekip_create.html', {
        'pokemons': all_pokemons,
        'selected_pokemons': selected_pokemons_object
    })


def add_pokemon(request, pokemon_id):
    global create_selected_pokemons
    create_selected_pokemons.append(pokemon_id)
    return redirect('../../../ekip/create/')

def del_pokemon(request, pokemon_id):
    global create_selected_pokemons
    create_selected_pokemons.remove(pokemon_id)
    return redirect('../../../ekip/create/')


def save_ekip(request):
    global create_selected_pokemons
    global ekip_id_seq
    if request.method == 'POST':
        form = NameTeamForm(request.POST)
        if form.is_valid():
            selected_pokemons_object = []
            for select_id in create_selected_pokemons:
                for poke in all_pokemons:
                    if poke.objectId == select_id:
                        selected_pokemons_object.append(poke)

            ekip_list.append(Ekip(
                ekip_id_seq,
                str(form.data['name']),
                selected_pokemons_object
            ))
            ekip_id_seq += 1
            create_selected_pokemons = []

    return redirect('../../ekips')


def set_selected_ekip(request, ekip_id):
    global selected_ekip
    for e in ekip_list:
        if e.objectId == ekip_id:
            selected_ekip = e

    return redirect('..')


def pekomens(request):
    global all_pokemons
    global searched_pokemons
    if len(all_pokemons) <= 0:
        response_list = requests.get('https://pokeapi.co/api/v2/pokemon/?limit=151')
        pokemon_list = response_list.json()
        for global_pokemon in pokemon_list["results"]:
            response_details = requests.get(global_pokemon["url"])
            pokemon_details = response_details.json()
            all_pokemons.append(
                Pokemon(
                    global_pokemon["name"],
                    pokemon_details["sprites"]["other"]["home"]["front_default"],
                    pokemon_details["id"]
                )
            )
        searched_pokemons = all_pokemons

    return render(request, 'pekomens.html', {
        'pokemons': searched_pokemons
    })


def search(request):
    global all_pokemons
    global searched_pokemons
    searched_pokemons = []
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            keyword = str(form.data['search'])
            if not keyword or keyword == '':
                searched_pokemons = all_pokemons
            else:
                for poke in all_pokemons:
                    if str(form.data['search']) in poke.name:
                        searched_pokemons.append(poke)
        else:
            searched_pokemons = all_pokemons

    return redirect('../..')


def pekomen(request, pokemon_id):
    pokemon_listt = requests.get('https://pokeapi.co/api/v2/pokemon/' + str(pokemon_id))
    pokemon_d = pokemon_listt.json()
    abilities = []
    types = []
    stats = []
    sprites = pokemon_d["sprites"]["other"]["home"]["front_default"]
    for ability in pokemon_d["abilities"]:
        abilities.append(ability["ability"]["name"])
    for type in pokemon_d["types"]:
        types.append(type["type"]["name"])
    for stat in pokemon_d["stats"]:
        stats.append(
            Pokemon_detail_stat(
                stat["stat"]["name"],
                stat["base_stat"],
            )
        )
    pekomen_detail = Pokemon_detail(
            pokemon_id,
            pokemon_d["name"],
            abilities,
            types,
            stats,
            sprites,
            pokemon_d["weight"],
            pokemon_d["height"],
            pokemon_d["base_experience"],
        )
    print(pekomen_detail.types)
    return render(request, 'pekomen.html', {
        'pokemon': pekomen_detail,
        'next': pokemon_id + 1 ,
        'previous': pokemon_id - 1
    })